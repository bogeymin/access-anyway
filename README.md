# Access Anyway

It often happens when developing or maintaining a website that I need to
disable access to the site, but still be able to access it myself. If the site
I'm working on doesn't have a maintenance mode feature, I need some way to
access the site in spite of the fact that it is down. Sometimes I also need to
give this access to a client or technician as well. This is what Access Anyway
is for.

## Requirements

At the moment, *Access Anyway* is written in PHP and the include file which
checks for current access assumes you wish to access a PHP-based web site. You
*could* use *Access Anyway* on a non-PHP site provided you a) have the ability
to run PHP on the server, and b) your code supports sessions. See
`includes/access.php` for how this is set up. Also see *Using Access Anyway
with Non-PHP Applications* below.

## Installation

Download the source code, or better yet, check it out like so:

    git clone git@bitbucket.org:bogeymin/access-anyway.git

Put this any where that is accessible to your web server, either in document
root or as an alias.

## Usage

Using *Access Anyway* is really easy. After downloading the code, insert this
at the top of your PHP file:

    include "access-anyway/includes/access.php"; # or the path to where you put it

By default, this will simply inform the visitor that the site is down. In order
to access the site anyway, got to `/url/to/access-anyway` and click on the
appropriate button. Once the cookie is set, future calls to
`includes/access.php` will have no effect.

### Using Access Anyway with Non-PHP Applications

If it possible to use *Access Anyway* outside of PHP. For example, you could
set up *Access Anyway* in a Web-accessible directory or using an alias. The
application will still work as normal. However, since you can't include the
`access.php` file, you will need to implement the cookie check in your own
code.

### Central Location

Access Anyway may be used and customized from your project, but you can also
maintain a copy in a central location on your server which may be used for
multiple projects.

Assuming you installed Access Anyway at `/opt/access-anyway`, the following
setup and usage would be possible:

1. Your Web server must be set up with an alias to serve `/access-anyway`. For
   example, with Apache, this would be:
   `Alias /access-anyway "/opt/access-anyway"`
2. Change to the document root of your site or project.
3. Run the `/opt/access-anyway/setup.sh`. This command creates
   `../config/access-anyway/config.php`.
4. Edit the `config.php` file that was just created to change the settings to
   your liking.
5. Edit any file that needs to check access (like `index.php`) and add
   `include "/opt/access-anyway/includes/access.php";` to the top of the file.

> Note: If you have command line access, the setup.sh script is provided for
convenience, but is based on a specific implementation. If this isn't to your
liking, feel free to customize the script. Also, you may enter 
`./setup.sh --help` on the command line for information about the script.

## Configuration

Currently only one configuration option is supported:

    $REDIRECT_URL = "http://example.com/503.php";

The `includes/access.php` file looks for `config.php` in these locations and in
the following order:

1. The path to the `includes/` directory (where `access.php` lives).
2. The path to the `access-anyway/index.php` file.
3. A `../config/access_anyway` directory just above document root.

## Disclaimer About the Image

I'm using an image for the offline access page (under-the-hood.jpg), but I'm
not sure where it came from or what it's licensing restrictions would be. If
this is your image and you would like credit for it, please let me know. Or let
me know if you want it removed from the project. If this is not your image, but
you have a better one to submit with a more certain heritage, then please send
a pull request.
