<?php

// Set defaults.
$COOKIE_PATH = "/";
$COOKIE_TIME = time()+ 2582000; # now plus 30 days
$IMAGE_URL = '/access-anyway/assets/images/under-the-hood.jpg';
$REDIRECT_URL = '/coming-soon';
$USE_SESSION = FALSE;

// Pre-determined determined locations for the config file.
$CONFIG_LOCATIONS = array(
    'include_path' => dirname(__FILE__).'/../includes',
    'root_path' => dirname($_SERVER['SCRIPT_FILENAME']).'/../',
    'config_directory' => sprintf('%s/../config/access_anyway', $_SERVER['DOCUMENT_ROOT']),
    'config_path' => sprintf('%s/../config/access-anyway', $_SERVER['DOCUMENT_ROOT']),
);

// Load the configuration file, if it can be found.
foreach ($CONFIG_LOCATIONS as $path)
{
    if (is_file("$path/config.php")) 
    { 
        include "$path/config.php";
    }
}
