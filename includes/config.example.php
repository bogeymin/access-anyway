<?php

/* The default path for the cookie is "/" which allows it to be used across the
whole site. You can override this be more or less restrictive. For example,
".example.com" would allow the cookie to be used across all the subdomains of
the site, while "/path/to/some/directory" would case the cookie only to work
for the given URI.
*/
$COOKIE_PATH = ".example.com";

/* By default the cookie expires 30 days from assignment. You may override this
here.
*/
$COOKIE_TIME = time() + 3600; # an hour from now

/* The URL to use on the Access Anyway page. The default assumes that an alias 
has been set up for /access-anyway.
*/
$IMAGE_URL = '/access-anyway/assets/images/under-the-hood.jpg';


/* When maintenance access is disabled (the default), redirect users to this URL.
*/
$REDIRECT_URL = "/url/to/maintenance/page";

/* When USE_SESSION is TRUE, Access Anyway will use a PHP session to store access info rather than an individual cookie. This means that $COOKIE_ settings are ignored, but may be more reliable in some circumstances.
*/
$USE_SESSION = FALSE;
