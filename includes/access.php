<?php

// Load configuration data.
include dirname(__FILE__)."/load-config.php";

// If access has been enabled, simply return processing back to the caller.
if ($USE_SESSION)
{
    session_start();
    if ($_SESSION['access_anyway']) { return; }
    if (!$_SESSION['access_anyway'])
    {
        header("Location: $REDIRECT_URL");
        die();
    }
}
else
{
    if ($_COOKIE['access_anyway']) { return; }
}

// Redirect the user if access has not been enabled.
header("Location: $REDIRECT_URL");
die("This site is currently down for maintenance.");
