#! /bin/bash

###################################
# Configuration
###################################

# The current working directory is used for a basic of other activity. Set this
# here before the directory is changed.
CURRENT_PATH=`pwd`;

# We need to make sure that everything starts relative to the same directory as
# this script.
SCRIPT_PATH="`readlink -f $0 | xargs dirname`";
cd $SCRIPT_PATH;

# Script information.
DATE="20 Aug 2012";
SCRIPT=`basename $0`;
VERSION="0.1.0-d";

# Exit codes.
EXIT_NORMAL=0;
EXIT_USAGE=1;
EXIT_ENVIRONMENT=2;
EXIT_OTHER=3;

# Temporary file.
# TEMP_FILE="$SCRIPT".$$

###################################
# Help
###################################

HELP="
SUMMARY

Convenience script for setting up Access Anyway for a particular project.

OPTIONS

-h		
    Print help and exit.

--help
    Print more help and exit.

-v		
    Print version and exit.

--version
    Print full version and exit.

NOTES

To make sure the paths are correct, you should run this script from the
document root of your site or project.
";

# Help and information.
if [[ $1 = '--help' ]]; then echo "$HELP"; exit $EXIT_NORMAL; fi;
if [[ $1 = '--version' ]]; then echo "$SCRIPT $VERSION ($DATE)"; exit $EXIT_NORMAL; fi;

# Make sure we aren't running from ./setup.sh because that could cause
# problems.
if [[ $CURRENT_PATH == $SCRIPT_PATH ]]; then
    echo "You should run this script from the document root directory of your project.";
    exit $EXIT_USAGE;
fi;

###################################
# Arguments
###################################

while getopts "hv" arg
do
    case $arg in
        v) echo "$VERSION"; exit;;
        h|*) echo "$SCRIPT [OPTIONS]";;
    esac
done

###################################
# Procedure
###################################

# Create the config directory as needed.
if [[ ! -d "$CURRENT_PATH/../config/access-anyway" ]]; then
    echo "Creating configuration directory $CURRENT_PATH/../config/access-anyway ...";
    mkdir -p $CURRENT_PATH/../config/access-anyway;
fi;

# Create the configuration script.
cp $SCRIPT_PATH/includes/config/config.example.php $CURRENT_PATH/../config/access-anyway/config.php;

# Log the activity.
# logger -i -t $SCRIPT "$SUDO_USER {COMMENT}";

# Clean up and exit.
# if [[ -f $TEMP_FILE ]]; then rm $TEMP_FILE; fi;
exit $EXIT_NORMAL;

# vim: set foldmethod=marker:
