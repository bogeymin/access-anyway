<?php

// Load configuration data.
include "includes/load-config.php";

// Find out where we are in the file system and URL structure.
$current_path = dirname($_SERVER['SCRIPT_FILENAME']);
$current_url = dirname($_SERVER['PHP_SELF']);

// Collect access information.
$access_anyway = FALSE;
if ($USE_SESSION)
{
    session_start();
    $access_anyway = $_SESSION['access_anyway'];

    if (isset($_SESSION['access_anyway_key']))
    {
        $current_key = $_SESSION['access_anyway_key'];
    }
    else
    {
        $current_key = uniqid();
        $_SESSION['access_anyway_key'] = $current_key;
    }
}
else
{

    $access_anyway = $_COOKIE['access_anyway'];

    if (isset($_COOKIE['access_anyway_key']))
    {
        $current_key = $_COOKIE['access_anyway_key'];
    }
    else
    { 
        $current_key = uniqid();
        setcookie('access_anyway_key', $current_key, $COOKIE_TIME, $COOKIE_PATH);
    }
}

?>
<!DOCTYPE html>
<html>
<head>		
    <meta charset="utf-8">
    <title>Offline Access via Access Anyway</title>
    <link rel="stylesheet" href="<?=$current_url?>/assets/css/default.css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){

        // Add access.
        $('#access').click(function(){
            $.get('<?= $current_url ?>/scripts/access.php?key=<?=$current_key?>')
            .success(function(data){
                    alert('You have been added to the access list.');
                    window.location.reload();
            });
        });

        // Forget access.
        $('#forget').click(function(){
            $.get('<?= $current_url ?>/scripts/forget.php?key=<?=$current_key?>')
            .success(function(data) {
                alert('You have been removed from the access list.');
                window.location.reload();
            });
        });
    });
    </script>
    <!--[if IE 6]>
        <script src="<?=$current_url?>/assets/js/ie6-transparency.js"></script>
        <script>
            DD_belatedPNG.fix('#wrap .line, #header #header-photo');
        </script>
        <link rel="stylesheet" href="<?=$current_url?>/assets/css/ie6.css" />
    <![endif]--> 
    <!--[if IE 7]>
        <link rel="stylesheet" href="<?=$current_url?>/assets/css/ie7.css" />
    <![endif]--> 
    <!--[if IE 8]>
        <link rel="stylesheet" href="<?=$current_url?>/assets/css/ie8.css" />
    <![endif]--> 	
    <!--[if IE 9]>
        <link rel="stylesheet" href="<?=$current_url?>/assets/css/ie9.css" />
    <![endif]--> 	
</head>
<body>	
    <div id="wrap">
        <div id="content">
            <div id="info" class="section">
                <img class="right embelish" src="<?=$IMAGE_URL?>" />
                <h2>Offline Access</h2>
                <p>
                This site is under construction or down for maintenance.
                </p>
                <p>
                However, if you wish to access this site anyway, please click 
                the button below.
                </p>
                <p>
                <button id="access" title="click to access the site in spite of maintenance mode">Access Anyway</button> 
                <button id="forget" title="click to access the site normally">Forget Access</button>
                </p>
            </div>
            <div class="line"></div>
            <div id="status" class="section">
                <p>
                Your current status is:
                </p>
                <p>
                    <?php if ($access_anyway): ?>
                    <big class="access">Maintenance Access</big>
                    <?php else: ?>
                    <big class="noaccess">Normal Access</big>
                    <?php endif ?>
                </p>
            </div>
        </div><!--end content-->
    </div><!--end wrap-->
    <div id="spacing"></div><!--hack for ie6 and ie7-->	
</body>	
</html>
