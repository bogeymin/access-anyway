<?php

// Don't do anything if POST doesn't include a key.
if (!isset($_GET['key'])) { die(); }

// Load the configuration data.
include "../includes/load-config.php";

// Enable access.
if ($USE_SESSION)
{
    session_start();
    $_SESSION['access_anyway'] = TRUE;
}
else
{
    setcookie('access_anyway', TRUE, $COOKIE_TIME, $COOKIE_PATH);
}
