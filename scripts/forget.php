<?php

// Don't do anything if POST doesn't include a key.
if (!isset($_GET['key'])) { die(); }

// Load the configuration data.
include "../includes/load-config.php";

// Disable access.
if ($USE_SESSION)
{
    session_start();
    unset($_SESSION['access_anyway']);
    unset($_SESSION['access_anyway_key']);
}
else
{
    setcookie('access_anyway', '', time() - 3600, "/");
    setcookie('access_anyway_key', '', time() - 3600, "/");
}
